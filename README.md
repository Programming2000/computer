# Computer

Project in EDAF60
A simulation of a computer machine.

Förberedelsefrågor:

Upg 1. 

Abstrakta klasser:
    Program

Interface:
    Word
    WordFactory
    Instruction
    
Klasser:
    ByteWord
    LongWord
    ByteWordFactory
    LongWordFactory
    Computer
    Memory
    Copy
    Add
    Mul
    Jump
    JumpEq
    Halt
    Print
    Adress
    ProgramCounter
    Factorial
    Sum
    
Upg 2.

Man kan implementera den genom att extenda till exempel ArrayList.
Detta skulle förenkla arbetet för att metoder och atribut redan finns
implementerade. Det skulle dock bidra med väldigt mycket som inte behövs
i vårt fall, vilket både skulle sakta ner programmet, men framför allt göra 
programmet svårare att arbeta på. Bättre skulle vara att implementera
en egen listklass.

Upg 3.

Vi vill ha två stora paket som klasserna ligger i. Allt grundläggande som behövs
för att kunna använda datorn finns i ett paket, det vill säga:
    -Computer
    -Memory
    -Address
    -ProgramCounter
    -Instruction
    -Program
    
Det andra paketet är det valbara, som innehåller all funktionalitet men som
inte är nödvändigt ifall man vill skriva sina egna program, alltså:
    -Factorial
    -Sum
    -Copy
    -Add
    -Mul
    -Jump
    -JumpEq
    -Halt
    -Print
    -ByteWord
    -LongWord
    -ByteWordFactory
    -LongWordFactory
    
Upg 4.

Vi avänder Instruction-Klassen enligt Commandmetoden för att samla information
om en aktion. Till exempel för add, copy och liknande.

Upg 5.

Till exempel instruktionerna Add och Mul liknar varandra på många sätt och 
skulle kunna implementeras med hjälp av Template Method.

Upg 6.

Vi tror att användandet av interfacet Instruction är ett exempel på strategy.
Computer anropar metoden execute() på instruktioner, men lämnar ansvaret för hur 
detta går till hos klasserna själva. Även LongWordFactory och ByteWordFactory
fungerar på liknande sätt, metoden value(String word) anropas på WordFactorys 
och implementationerna av det interfacet sköter själva den uppgiften.

Upg 7.

I Add-klassen.

Upg 9.

Vi bör kasta ett typfel. 